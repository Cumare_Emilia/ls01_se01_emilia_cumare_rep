/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
        byte anzahlPlaneten =  9 ;
    
    // Anzahl der Sterne in unserer Milchstraße
        long anzahlSterne =  100000000000L ;
    
    // Wie viele Einwohner hat Berlin?
       int bewohnerBerlin = 3769000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
       int alterTage = 7665;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm = 190000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
       long flaecheGroessteLand =  2400000;
    
    // Wie groß ist das kleinste Land der Erde?
    
       float flaecheKleinsteLand =  0.44f;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne " + anzahlSterne);
    
    System.out.println ("Anzahl Einwohner in Berlin" + bewohnerBerlin );
    System.out.println ("schwerste Tier der Welt" + gewichtKilogramm);
    System.out.println ("Groeste Land der Erde" + flaecheGroessteLand);
    System.out.println ("kleinste Land der Erde" + flaecheKleinsteLand);
     
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

